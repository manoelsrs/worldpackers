package com.manoelsrs.worldpackers

import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.runner.AndroidJUnit4
import com.manoelsrs.worldpackers.core.http.WorldpackersApi
import com.manoelsrs.worldpackers.espresso.onView
import com.manoelsrs.worldpackers.espresso.type
import com.manoelsrs.worldpackers.main.MainActivity
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class InstrumentedTest {

    @Rule
    @JvmField
    val rule = IntentsTestRule(
            MainActivity::class.java,
            false,
            false
    )

    private val server = MockWebServer()

    private fun launchActivity() {
        rule.launchActivity(null)
    }

    @Before
    fun before() {
        server.start()
        WorldpackersApi.setUrl(server.url("/").toString())
    }

    @Test
    fun testFlow() {
        startServer(listOf(MockResponse().setBody(RioJson.rioJson)))
        launchActivity()

        R.id.searchET.onView()
                .type("rio")

        //insert tests for recycler here
    }

    private fun startServer(mockResponse: List<MockResponse>) {
        mockResponse.forEach {
            server.enqueue(it)
        }
    }
}