package com.manoelsrs.worldpackers

object RioJson {

    const val rioJson = """
        {
            "total_hits": 3,
            "next_page_url": null,
            "prev_page_url": null,
            "hits": [
                {
                    "id": 158,
                    "title": "Super Vaga demais",
                    "city": "Brochier",
                    "country": "Brazil",
                    "rating": null,
                    "reviews_count": 0,
                    "photo_url": "https://s3.amazonaws.com/worldpackers_staging/volunteer_positions/photos/000/000/158/main/f3ccdd27d2000e3f9255a7e3e2c48800.jpg",
                    "price": 0,
                    "url": "https://staging-worldpackersplatform.herokuapp.com/api/volunteer_positions/158",
                    "accommodation_type_slug": "shared_dorm",
                    "recent_applicants": 0,
                    "meals_count": 1,
                    "trips_count": 0,
                    "hosting_since": "2018-06-11",
                    "last_minute_active": false
                },
                {
                    "id": 133,
                    "title": "An awesome title is unique and exciting! ",
                    "city": "Rio de Janeiro",
                    "country": "Brazil",
                    "rating": null,
                    "reviews_count": 0,
                    "photo_url": "/photos/main/missing.png",
                    "price": 5000,
                    "url": "https://staging-worldpackersplatform.herokuapp.com/api/volunteer_positions/133",
                    "accommodation_type_slug": "private_room",
                    "recent_applicants": 0,
                    "meals_count": 2,
                    "trips_count": 0,
                    "hosting_since": "2017-07-04",
                    "last_minute_active": false
                },
                {
                    "id": 159,
                    "title": "Outra vaga irada",
                    "city": "Brochier",
                    "country": "Brazil",
                    "rating": null,
                    "reviews_count": 0,
                    "photo_url": "https://s3.amazonaws.com/worldpackers_staging/volunteer_positions/photos/000/000/159/main/8cda81fc7ad906927144235dda5fdf15.jpg",
                    "price": 0,
                    "url": "https://staging-worldpackersplatform.herokuapp.com/api/volunteer_positions/159",
                    "accommodation_type_slug": "shared_dorm",
                    "recent_applicants": 0,
                    "meals_count": 1,
                    "trips_count": 0,
                    "hosting_since": "2018-06-11",
                    "last_minute_active": false
                }
            ],
            "experiences": []
        }
    """
}