package com.manoelsrs.worldpackers.adapter

import com.manoelsrs.worldpackers.model.Model

data class OpportunitiesViewModel(private val opportunities: Model.Opportunities) : SearchViewModel {
    override val raw = opportunities
    override val imageURL = opportunities.photoUrl
    override val title = opportunities.country
    override val subTitle = opportunities.city
    override val description = opportunities.title
}