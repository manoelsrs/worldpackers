package com.manoelsrs.worldpackers.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.manoelsrs.worldpackers.R

class SearchListAdapter : RecyclerView.Adapter<SearchViewHolder>() {

    internal var items: List<SearchViewModel> = arrayListOf()

    internal lateinit var onClick: (Any) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.search_results, parent, false)
        return SearchViewHolder(view)
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
            holder.onBind(items[position], onClick)
    }
}