package com.manoelsrs.worldpackers.adapter

import com.manoelsrs.worldpackers.model.Model

fun SearchListAdapter.setOpportunities(newContent: List<Model.Opportunities>, onClick: (Model.Opportunities) -> Unit) {
    this.onClick = onClick as (Any) -> Unit
    items = newContent.map {
        OpportunitiesViewModel(it)
    }
    notifyDataSetChanged()
}