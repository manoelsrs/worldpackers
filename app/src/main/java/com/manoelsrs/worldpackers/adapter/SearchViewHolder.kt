package com.manoelsrs.worldpackers.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.manoelsrs.worldpackers.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.search_results.view.*
import java.lang.Exception

class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun onBind(viewModel: SearchViewModel, onClick: (Any) -> Unit) {
        viewModel.apply {
            Picasso.get()
                    .load(imageURL)
                    .error(R.mipmap.ic_launcher)
                    .into(itemView.photo, object : Callback {
                        override fun onSuccess() {
                            itemView.loadingPB.visibility = View.GONE
                        }

                        override fun onError(e: Exception?) {
                            itemView.photo.setImageResource(R.mipmap.ic_launcher)
                        }
                    })

            itemView.countryName.text = title
            itemView.cityName.text = subTitle
            itemView.title.text = description

            itemView.setOnClickListener { onClick(viewModel.raw) }
        }
    }
}