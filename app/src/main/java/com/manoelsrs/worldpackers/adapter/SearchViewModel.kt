package com.manoelsrs.worldpackers.adapter

interface SearchViewModel {
    val raw: Any
    val imageURL: String
    val title: String
    val subTitle: String
    val description: String
}