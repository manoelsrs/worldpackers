package com.manoelsrs.worldpackers.core.http

object HttpConstants {
    const val baseUrl = "https://staging-worldpackersplatform.herokuapp.com/api/"
    const val accept = "Accept: application/vnd.worldpackers.com.v11+json"
    const val authorization = "Authorization: bearer 9e5a86cfca45eba00668e1baf15fd8dd65c15ad760e00845b81995d242844cdd"
    const val acceptLanguage = "Accept-Language: 'en-US'"
    const val qGet = "search"
}