package com.manoelsrs.worldpackers.core.http

import com.manoelsrs.worldpackers.model.Model
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface Service {

    @Headers(
        HttpConstants.accept,
        HttpConstants.authorization,
        HttpConstants.acceptLanguage
    )
    @GET(HttpConstants.qGet)
    fun listHits(
            @Query("page") page: String,
            @Query("per_page") perPage: String,
            @Query("q") mySearch: String
    ): Observable<Model.Hits>
}