package com.manoelsrs.worldpackers.core.utils

import io.reactivex.Completable
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit

class CompletableUtils {

    fun completeAfter(millis: Long, scheduler: Scheduler): Completable =
            Completable.complete()
                    .delay(millis, TimeUnit.SECONDS, scheduler)
}