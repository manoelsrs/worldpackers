package com.manoelsrs.worldpackers.core.utils

import android.content.Context
import android.net.ConnectivityManager

fun Context.isNetworkAvailable(): Boolean {
    return connectivityManager().activeNetworkInfo?.let {
        it.isAvailable and it.isConnected
    } ?: false
}

private fun Context.connectivityManager() =
        this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager