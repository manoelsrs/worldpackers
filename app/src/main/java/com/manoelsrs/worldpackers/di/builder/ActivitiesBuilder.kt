package com.manoelsrs.worldpackers.di.builder

import com.manoelsrs.worldpackers.main.MainActivity
import com.manoelsrs.worldpackers.splash.SplashActivity
import com.manoelsrs.worldpackers.di.PerActivity
import com.manoelsrs.worldpackers.di.module.MainActivityModule
import com.manoelsrs.worldpackers.di.module.SplashActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesBuilder {

    @PerActivity
    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashActivity

    @PerActivity
    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    abstract fun bindMainActivity(): MainActivity
}