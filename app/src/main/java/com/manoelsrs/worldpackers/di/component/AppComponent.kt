package com.manoelsrs.worldpackers.di.component

import com.manoelsrs.worldpackers.MyApp
import com.manoelsrs.worldpackers.di.builder.ActivitiesBuilder
import com.manoelsrs.worldpackers.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ActivitiesBuilder::class
))
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MyApp): Builder

        fun build(): AppComponent
    }

    fun inject(app: MyApp)
}