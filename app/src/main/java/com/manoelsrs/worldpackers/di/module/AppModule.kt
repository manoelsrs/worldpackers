package com.manoelsrs.worldpackers.di.module

import android.app.Application
import com.manoelsrs.worldpackers.MyApp
import com.manoelsrs.worldpackers.core.scheduler.RxScheduler
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplication(app: MyApp): Application = app

    @Provides
    @Singleton
    fun provideRxScheduler(): RxScheduler = RxScheduler()
}