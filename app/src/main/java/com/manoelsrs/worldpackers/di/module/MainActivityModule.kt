package com.manoelsrs.worldpackers.di.module

import com.manoelsrs.worldpackers.main.MainActivity
import com.manoelsrs.worldpackers.main.MainController
import com.manoelsrs.worldpackers.main.MainView
import com.manoelsrs.worldpackers.core.scheduler.RxScheduler
import com.manoelsrs.worldpackers.di.PerActivity
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_main.*

@Module
class MainActivityModule {

    @PerActivity
    @Provides
    fun provideMainController(
            activity: MainActivity,
            rxScheduler: RxScheduler,
            mainView: MainView
    ) =
            MainController(
                    mainView,
                    rxScheduler,
                    activity,
                    mainView.getListening()
            )

    @PerActivity
    @Provides
    fun provideUI(activity: MainActivity) = MainView(activity.mainLayout)
}