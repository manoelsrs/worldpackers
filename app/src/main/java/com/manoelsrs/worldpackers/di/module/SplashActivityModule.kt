package com.manoelsrs.worldpackers.di.module

import com.manoelsrs.worldpackers.splash.SplashActivity
import com.manoelsrs.worldpackers.splash.SplashController
import com.manoelsrs.worldpackers.core.scheduler.RxScheduler
import com.manoelsrs.worldpackers.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
class SplashActivityModule {

    @PerActivity
    @Provides
    fun provideSplashPresenter(activity: SplashActivity, scheduler: RxScheduler) =
            SplashController(activity, scheduler)
}