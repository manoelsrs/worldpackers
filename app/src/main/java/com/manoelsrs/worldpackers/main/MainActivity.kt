package com.manoelsrs.worldpackers.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.manoelsrs.worldpackers.R
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainController: MainController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)

        mainController.onCreate()
    }

    override fun onDestroy() {
        mainController.onDestroy()
        super.onDestroy()
    }
}