package com.manoelsrs.worldpackers.main

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.manoelsrs.worldpackers.R
import com.manoelsrs.worldpackers.core.http.WorldpackersApi
import com.manoelsrs.worldpackers.core.http.friendlyMessage
import com.manoelsrs.worldpackers.core.scheduler.RxScheduler
import com.manoelsrs.worldpackers.core.utils.isNetworkAvailable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.toast
import retrofit2.HttpException

class MainController(
        internal val view: MainView,
        private val scheduler: RxScheduler,
        private val activity: Activity,
        private val scrollListening: Observable<Search>
) {

    private val disposable = CompositeDisposable()

    fun onCreate() {
        view.onCreate()
        bindET()
        disposable.add(scrollListening.subscribe { search(it.page,it.search) })
    }

    fun onDestroy() {
        disposable.clear()
    }

    private fun bindET() {
        disposable.add(
                view.searchListening
                        .observeOn(scheduler.mainThread())
                        .subscribe {
                            if (activity.isNetworkAvailable()) {
                                view.apply {
                                    cleanCache()
                                    reset(it)
                                }
                                search(1,it)
                            }
                            else {
                                showErrorMessage(R.string.noConnection)
                            }
                        }
        )
    }

    private fun search(page: Int, content: String) {
        view.setProgressBar(true)
        disposable.add(WorldpackersApi
                .loadOpportunities(page,content)
                .subscribeOn(scheduler.backgroundThread())
                .observeOn(scheduler.mainThread())
                .subscribe ({   model ->
                                view.apply {
                                    setOpportunities(model.hits) {
                                        startBrowser(it.url)
                                    }
                                    calcTotalPages(model.totalHits)
                                }
                            },
                            {
                                when(it){
                                    is HttpException -> it.friendlyMessage()

                                     else -> R.string.noConnection

                                }.apply { showErrorMessage(this) }
                            })
        )
    }

    private fun startBrowser(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        activity.startActivity(intent)
    }

    private fun showErrorMessage(message: Int) = activity.toast(message)
}