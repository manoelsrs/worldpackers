package com.manoelsrs.worldpackers.main

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.jakewharton.rxbinding2.widget.RxTextView
import com.manoelsrs.worldpackers.adapter.SearchListAdapter
import com.manoelsrs.worldpackers.adapter.setOpportunities
import com.manoelsrs.worldpackers.core.utils.EndlessRecyclerViewScrollListener
import com.manoelsrs.worldpackers.model.Model
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.activity_main.view.*
import java.util.concurrent.TimeUnit


class MainView(private val rootView: View) {

    private val listAdapter = SearchListAdapter()

    private var cache: List<Model.Opportunities> = mutableListOf()
    private val linearLayoutManager = LinearLayoutManager(rootView.context)

    private var totalPages = 1
    private var currentPage = 1
    private var mySearch = ""

    val searchListening: Observable<String> by lazy {
        RxTextView
                .textChanges(rootView.searchET)
                .debounce(400,TimeUnit.MILLISECONDS)
                .map { charSequence -> charSequence.toString() }
                .distinctUntilChanged()
    }

    private val scrollListener: EndlessRecyclerViewScrollListener by lazy {
        object : EndlessRecyclerViewScrollListener(linearLayoutManager){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (totalPages >= currentPage) {
                    currentPage++
                    scrollListening.onNext(Search(currentPage,mySearch))
                }
            }
        }
    }

    private val scrollListening = BehaviorSubject.create<Search>()

    fun getListening(): Observable<Search> = scrollListening

    fun onCreate() = rootView.searchList.apply {
        layoutManager = linearLayoutManager
        adapter = listAdapter
        addOnScrollListener(scrollListener)
    }

    fun setOpportunities(newCache: List<Model.Opportunities>, onClick: (Model.Opportunities) -> Unit) {
        cache += newCache
        listAdapter.setOpportunities(cache, onClick)
        setProgressBar(false)
    }

    fun setProgressBar(flag: Boolean) = rootView.apply{
        if (flag) progressBar.visibility = View.VISIBLE
        else progressBar.visibility = View.GONE
    }

    fun cleanCache() {
        cache = mutableListOf()
    }

    fun reset(content: String) {
        scrollListener.resetState()
        currentPage = 1
        mySearch = content
    }

    fun calcTotalPages(content: Int) {
        totalPages = Math.ceil(content/20.0).toInt()
    }
}