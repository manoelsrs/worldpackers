package com.manoelsrs.worldpackers.model

import com.google.gson.annotations.SerializedName

object Model {

    data class Hits(
            @SerializedName("total_hits") val totalHits: Int,
            val hits: List<Opportunities>,
            @SerializedName("next_page_url") val nextPageUrl: String?
    )

    data class Opportunities(
            val title: String,
            val city: String,
            val country: String,
            @SerializedName("photo_url") val photoUrl: String,
            val url: String
    )
}