package com.manoelsrs.worldpackers.splash

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.manoelsrs.worldpackers.R
import dagger.android.AndroidInjection
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var splashController: SplashController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        AndroidInjection.inject(this)

        splashController.onCreate()
    }

    override fun onDestroy() {
        splashController.onDestroy()
        super.onDestroy()
    }
}