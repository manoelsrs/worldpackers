package com.manoelsrs.worldpackers.splash

import com.manoelsrs.worldpackers.main.MainActivity
import com.manoelsrs.worldpackers.core.scheduler.RxScheduler
import com.manoelsrs.worldpackers.core.utils.CompletableUtils
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.startActivity

class SplashController(
        private val activity: SplashActivity,
        private val rxScheduler: RxScheduler) {

    private val disposable = CompositeDisposable()

    fun onCreate() = startMainActivity()

    private fun startMainActivity() {
        disposable.add(CompletableUtils()
                .completeAfter(2, rxScheduler.mainThread())
                .subscribeOn(rxScheduler.mainThread())
                .subscribe {
                    activity.apply {
                        startActivity<MainActivity>()
                        finish()
                    }
                }
        )

    }

    fun onDestroy() {
        disposable.clear()
    }
}