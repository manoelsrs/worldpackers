package com.manoelsrs.worldpackers.core.utils

import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.*
import org.junit.Test
import java.util.concurrent.TimeUnit

class CompletableUtilsTest {

    private val completableUtils = CompletableUtils()

    @Test
    fun testCompleteAfter() {
        val scheduler = TestScheduler()

        val testSubscriber = completableUtils
                .completeAfter(2,scheduler)
                .subscribeOn(scheduler)
                .test()

        scheduler.advanceTimeBy(1,TimeUnit.SECONDS)
        testSubscriber.assertNotComplete()

        scheduler.advanceTimeTo(2,TimeUnit.SECONDS)
        testSubscriber.assertComplete()
    }
}