package com.manoelsrs.worldpackers.model

import com.manoelsrs.worldpackers.mock.SuccessMock
import org.junit.Assert.*
import org.junit.Test

class ModelTest {

    private val rioSearch = SuccessMock.rioSearch

    @Test
    fun testSuccess() {

        val search = Model.Hits(
                3,
                listOf(
                        Model.Opportunities(
                                "Super Vaga demais",
                                "Brochier",
                                "Brazil",
                                "https://s3.amazonaws.com/worldpackers_staging/volunteer_positions/photos/000/000/158/main/f3ccdd27d2000e3f9255a7e3e2c48800.jpg",
                                "https://staging-worldpackersplatform.herokuapp.com/api/volunteer_positions/158"
                        ),
                        Model.Opportunities(
                                "An awesome title is unique and exciting! ",
                                "Rio de Janeiro",
                                "Brazil",
                                "/photos/main/missing.png",
                                "https://staging-worldpackersplatform.herokuapp.com/api/volunteer_positions/133"
                        ),
                        Model.Opportunities(
                                "Outra vaga irada",
                                "Brochier",
                                "Brazil",
                                "https://s3.amazonaws.com/worldpackers_staging/volunteer_positions/photos/000/000/159/main/8cda81fc7ad906927144235dda5fdf15.jpg",
                                "https://staging-worldpackersplatform.herokuapp.com/api/volunteer_positions/159"
                        )
                ),
                null
        )

        assertEquals(search,rioSearch)
    }
}